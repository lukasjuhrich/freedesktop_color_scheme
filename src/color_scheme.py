# coding: utf-8
import logging
import subprocess
import typing as t
from enum import Enum

import dbus
import typer
import rich as r
import rich.logging
import rich.text

logger = logging.getLogger(__name__)


def _setup_logging():
    logging.basicConfig(
        level="NOTSET",
        format="%(message)s",
        datefmt="[%X]",
        handlers=[r.logging.RichHandler(show_path=False)]
    )


class ColorScheme(str, Enum):
    Dark = "dark"
    Light = "light"


class ReportedColorScheme(Enum):
    NoPref = 0
    Dark = 1
    Light = 2


BUS = "org.freedesktop.portal.Desktop"
INTERFACE = "org.freedesktop.portal.Settings"
OBJECT_PATH = "/org/freedesktop/portal/desktop"


def get_color_scheme() -> ReportedColorScheme:
    session_bus = dbus.SessionBus()
    res = int(
        session_bus.call_blocking(
            bus_name=BUS,
            dbus_interface=INTERFACE,
            object_path=OBJECT_PATH,
            method="Read",
            signature="ss",
            args=("org.freedesktop.appearance", "color-scheme"),
        )
    )
    # see https://github.com/flatpak/xdg-desktop-portal/blob/d7a304a00697d7d608821253cd013f3b97ac0fb6/data/org.freedesktop.impl.portal.Settings.xml#L33-L45
    return ReportedColorScheme(res)


app = typer.Typer()


@app.command()
def show() -> None:
    """Show the current color scheme pref."""
    _setup_logging()
    text = r.text.Text()
    text.append("Current scheme: ")
    text.append(get_color_scheme().name, style="bold")
    r.console.Console().print(text)


#: TODO: Make configurable by TOML file
# you can view themes via `lookandfeeltool -l`
THEMES: t.Dict[ColorScheme, str] = {
    ColorScheme.Light: "org.kde.breeze.desktop",
    ColorScheme.Dark: "org.kde.breezedark.desktop",
}


def set_theme_kde(theme_name: str, logger: logging.Logger) -> None:
    args = ("lookandfeeltool", "-a", theme_name)
    logger.debug("Running %r", " ".join(args))
    subprocess.check_output(args)


@app.command()
def set(scheme: ColorScheme) -> None:
    """Set the color scheme explicitly"""
    _setup_logging()
    theme_name = THEMES[scheme]
    set_theme_kde(theme_name, logger)


@app.command()
def toggle() -> None:
    """toggle the color scheme."""
    _setup_logging()
    scheme: ReportedColorScheme = get_color_scheme()
    match scheme:
        case ReportedColorScheme.Light:
            new_scheme = ColorScheme.Dark
            logger.info(f"Current color scheme is {scheme.name}, setting {new_scheme}")
        case ReportedColorScheme.Dark:
            new_scheme = ColorScheme.Light
            logger.info(f"Current color scheme is {scheme.name}, setting {new_scheme}")
        case ReportedColorScheme.NoPref:
            new_scheme = ColorScheme.Light
            logger.info(f"No preference reported, settiang {new_scheme}")
        case _:
            assert False
    set(new_scheme)


if __name__ == "__main__":
    app()
